use super::strings;
use serde_derive::{Deserialize, Serialize};
use std::path::{Path, PathBuf};
#[derive(Deserialize, Serialize)]
pub struct Config {
    pub selected_path: PathBuf,
    pub initialized: bool,
    pub image_regex: Vec<String>,
    pub image_fields: toml::value::Table,
}

impl std::default::Default for Config {
    fn default() -> Self {
        let default_image_fields = {
            let mut table = toml::value::Table::new();
            table.insert("year".to_owned(), toml::Value::from(1));
            table.insert("month".to_owned(), toml::Value::from(1));
            table.insert("day".to_owned(), toml::Value::from(1));
            table.insert("hour".to_owned(), toml::Value::from(1));
            table.insert("minute".to_owned(), toml::Value::from(1));
            table.insert("second".to_owned(), toml::Value::from(1));
            table.insert("first_theme".to_owned(), toml::Value::from(1));
            table.insert("second_theme".to_owned(), toml::Value::from(1));
            table
        };

        Config {
            selected_path: PathBuf::default(),
            initialized: false,
            image_regex: vec![
                String::from(
                    r"(.*?)/(.*?)/(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)-(\\d\\d)h(\\d\\d)m(\\d\\d)\\..*",
                ),
                String::from(
                    r"(.*?)/(.*?)/(\\d\\d\\d\\d)(\\d\\d)(\\d\\d)_(\\d\\d)(\\d\\d)(\\d\\d)\\..*",
                ),
            ],
            image_fields: default_image_fields,
        }
    }
}

impl Config {
    pub fn read(path: &Path) -> Self {
        let process = || -> Result<Config, Box<dyn std::error::Error>> {
            let res = std::fs::read_to_string(path)?;
            toml::from_str(&res).map_err(Box::from)
        };

        process().unwrap_or_else(|e| {
            println!("{}\nErr: {}", strings::WARN_READ_CONFIG, e);
            let def = Self::default();
            Self::regen_file(&def, path).expect(strings::ERR_WRITE_CONFIG);
            def
        })
    }

    pub fn regen_file(&self, path: &Path) -> std::io::Result<()> {
        let str = toml::to_string(&self).expect(strings::ERR_CONFIG_TO_STRING);
        std::fs::write(path, &str)
    }
}
