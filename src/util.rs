use super::strings;
use std::{fmt::Display, io, str::FromStr};

fn input_helper<T, U>(message: &dyn Display, check: &dyn Fn(String) -> Result<T, U>) -> T
where
    U: std::fmt::Display,
{
    loop {
        println!("{}", message);

        let mut input = String::with_capacity(260); // Guess
        io::stdin()
            .read_line(&mut input)
            .expect(strings::ERR_READ_STDIN);

        input = input.trim().to_lowercase();

        match check(input) {
            Ok(result) => return result,
            Err(e) => {
                println!("{}\nErr: {}", strings::ERR_PARSE_INPUT, e);
            }
        }
    }
}

/// ### Performance
/// Allocates a new `String` every time it is called.
///
/// ### Panic
/// Panics if it fails to read from `Stdin`
pub fn input<T: FromStr>(message: &dyn Display) -> T
where
    <T as std::str::FromStr>::Err: std::fmt::Display,
{
    input_helper(message, &|i: String| i.parse::<T>())
}

/// ### Performance
/// Allocates a new `String` every time it is called.
///
/// ### Panic
/// Panics if it fails to read from `Stdin`
pub fn input_bool(message: &impl Display) -> bool {
    const YES: [&str; 3] = ["y", "yes", "true"];
    const NO: [&str; 3] = ["n", "no", "false"];

    input_helper(message, &|i: String| {
        if YES.iter().any(|&y| i == y) {
            return Ok(true);
        } else if NO.iter().any(|&n| i == n) {
            return Ok(false);
        }
        Err(format!("{} is invalid boolean", i))
    })
}
