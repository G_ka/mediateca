pub const STAGING_DIR: &str = "staging";
pub const STORE_DIR: &str = "store";
pub const DISPLAY_DIR: &str = "display";
pub const CONFIG_FILE: &str = "mediateca.toml";
