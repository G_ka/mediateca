use std::path::{Path, PathBuf};

use super::constant::{DISPLAY_DIR, STAGING_DIR, STORE_DIR};
use super::util;

pub enum DirType {
    Root,
    Staging,
    Store,
    Display,
}

fn strip_all_prefix<'a>(file: &'a Path, root_path: &Path) -> &'a Path {
    let one = file.strip_prefix(root_path).unwrap_or(file);
    let two = one.strip_prefix(DISPLAY_DIR).unwrap_or(one);
    let thr = two.strip_prefix(STAGING_DIR).unwrap_or(two);
    thr.strip_prefix(STORE_DIR).unwrap_or(thr)
}

fn get_path(root: &Path, dir_type: DirType, diff: &Path) -> PathBuf {
    match dir_type {
        DirType::Root => root.join(diff),
        DirType::Staging => root.join(STAGING_DIR).join(diff),
        DirType::Store => root.join(STORE_DIR).join(diff),
        DirType::Display => root.join(DISPLAY_DIR).join(diff),
    }
}

fn is_known_folder(file: &Path) -> bool {
    if let Some(name) = file.file_name() {
        return name == STAGING_DIR || name == DISPLAY_DIR || name == STORE_DIR;
    }
    false
}

pub fn safe_move(
    file: &Path,
    dir: DirType,
    root_path: &Path,
) -> Result<PathBuf, Box<dyn std::error::Error>> {
    if is_known_folder(file) {
        return Ok(PathBuf::from(file));
    }

    let diff = strip_all_prefix(&file, root_path);
    let new_path = get_path(root_path, dir, diff);

    if new_path.exists() {
        let text = format!(
            "{} already exists. Do you want to replace it with {}?",
            new_path.display(),
            file.display()
        );
        let ans = util::input_bool(&text);
        if !ans {
            return Ok(PathBuf::from(file));
        }
    }

    let parent = &new_path.parent();
    if let Some(p) = parent {
        std::fs::create_dir_all(&p)?;
    }

    std::fs::rename(file, &new_path)?;
    Ok(new_path)
}
