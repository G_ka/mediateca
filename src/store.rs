use std::path::Path;

use super::config::Config;
use super::constant;
use super::files;
use super::strings::{
    ERR_FILENAME_UTF8, ERR_INVALID_REGEX, ERR_INVALID_STORED_REGEX, ERR_NO_REGEX_SUCCEED,
    PROMPT_USE_REGEX,
};
use super::util;

use regex::Regex;
use walkdir::WalkDir;

fn parse_name(
    name: Option<&std::ffi::OsStr>,
    regex: &Regex,
    mut out: toml::value::Table,
) -> Result<toml::value::Table, Box<dyn std::error::Error>> {
    let name = name.and_then(|f| f.to_str()).ok_or(ERR_FILENAME_UTF8)?;
    let caps = regex.captures(name).ok_or(ERR_INVALID_REGEX)?;

    for (i, (_, val)) in out.iter_mut().enumerate() {
        let res = caps.get(i + 1).ok_or(ERR_INVALID_REGEX)?;
        *val = toml::Value::from(res.as_str());
    }

    Ok(out)
}

fn try_all_regex(
    name: Option<&std::ffi::OsStr>,
    regexs: &[Regex],
    out: toml::value::Table,
) -> Result<toml::value::Table, Box<dyn std::error::Error>> {
    for re in regexs {
        if let Ok(res) = parse_name(name, re, out.clone()) {
            return Ok(res);
        }
    }
    Err(Box::from(ERR_NO_REGEX_SUCCEED))
}

fn prompt_value(key: &str, default: &toml::Value) -> toml::Value {
    let text = format!("Value for {}: (empty for {})", key, default);
    let res = util::input::<String>(&text);
    if res.is_empty() {
        default.clone()
    } else {
        toml::Value::from(res)
    }
}

fn prompt_values(
    mut conf: toml::value::Table,
) -> Result<toml::value::Table, Box<dyn std::error::Error>> {
    for (key, val) in conf.iter_mut() {
        *val = prompt_value(key, val);
    }
    Ok(conf)
}

fn get_values(
    name: Option<&std::ffi::OsStr>,
    table: &toml::value::Table,
    regs: &[Regex],
) -> Result<toml::value::Table, Box<dyn std::error::Error>> {
    // TODO For now, we assume everything image and video share the same config

    let mut res = match &regs.is_empty() {
        false => try_all_regex(name, &regs, table.clone()),
        true => prompt_values(table.clone()),
    };

    while res.is_err() {
        println!(
            "An error occurred: {}\nPlease enter the values manually",
            res.unwrap_err()
        );
        res = prompt_values(table.clone())
    }
    res
}

fn store_file(
    root: &Path,
    file: &Path,
    info: toml::value::Table,
) -> Result<(), Box<dyn std::error::Error>> {
    let new_path = files::safe_move(file, files::DirType::Store, root)?;
    let info = toml::to_string(&info)?;
    std::fs::write(new_path.with_extension(".toml"), info)?;
    Ok(())
}

fn process_file(
    file: walkdir::DirEntry,
    regex: &[Regex],
    config: &Config,
) -> Result<(), Box<dyn std::error::Error>> {
    if file.file_type().is_dir() {
        return Ok(());
    }

    let path = file.path();
    println!("Processing {}", path.display());

    // TODO For now, we assume everything (image and video) share the same config
    let out = get_values(path.file_name(), &config.image_fields, regex)?;

    store_file(&config.selected_path, path, out)?;
    Ok(())
}

fn prompt_regex(config: &Config) -> Result<Vec<Regex>, Box<dyn std::error::Error>> {
    let regs = &config.image_regex;
    if !regs.is_empty() {
        let text = format!(
            "The current regexs are: {:#?}\nDo you want to keep them?",
            regs.is_empty()
        );
        let keep = util::input_bool(&text);
        if keep {
            return Ok({
                let mut res = Vec::with_capacity(regs.len());
                for re in regs {
                    res.push(Regex::new(&re).expect(ERR_INVALID_STORED_REGEX))
                }
                res
            });
        }
    }

    if !util::input_bool(&PROMPT_USE_REGEX) {
        return Ok(Vec::default());
    }

    let keys = config.image_fields.keys();

    let len = keys.len();
    let mut joined_keys = String::with_capacity(50);
    for k in keys {
        joined_keys += &format!("{} ", k)
    }

    let mut regs = Vec::default();
    loop {
        let text = format!(
            "What is the regex you want to use? You have to provide
    {} capture groups, in the following order: {}",
            len, joined_keys
        );
        regs.push(util::input::<Regex>(&text));
        if !util::input_bool(&"Another one?") {
            break;
        }
    }
    Ok(regs)
}

pub fn process_staging(config: &mut Config) -> std::io::Result<()> {
    println!("Processing staging");
    let re = prompt_regex(&config).unwrap_or_else(|e| {
        println!("An error occurred while asking for regex: {}\nSkipping", e);
        Vec::default()
    });
    config.image_regex = re.iter().map(|r| String::from(r.as_str())).collect();

    let path = config.selected_path.join(constant::STAGING_DIR);
    let iter = WalkDir::new(path).into_iter().filter_map(|e| e.ok());

    for f in iter {
        if let Err(e) = process_file(f, &re, config) {
            println!("An error: {}; skipped.", e);
            continue;
        }
    }
    Ok(())
}
