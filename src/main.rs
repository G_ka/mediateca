mod config;
mod constant;
mod files;
mod setup;
mod store;
mod strings;
mod util;

use config::Config;
use setup::setup;
use std::path::Path;
use store::process_staging;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut config = Config::read(Path::new(constant::CONFIG_FILE));
    setup(&mut config);
    config.regen_file(Path::new(constant::CONFIG_FILE))?;
    process_staging(&mut config)?;
    config.regen_file(Path::new(constant::CONFIG_FILE))?;
    Ok(())
}
