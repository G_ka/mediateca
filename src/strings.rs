pub const PROMPT_DIRECTORY: &str =
    "Which directory do you want to use? (relative dirs allowed)";

pub const PROMPT_MOVE_EXISTING: &str =
    "Do you want to move existing directories? (true / false)";

pub const ERR_CREATE_DIR: & str =
    "An error occurred while creating a directory. Please close other applications and try again.";

pub const ERR_PARSE_INPUT: &str = "An error occurred while parsing input.";

pub const ERR_READ_STDIN: &str = "An error occurred while reading from stdin.";

pub const ERR_MOVE_EXISTING: & str = "An error occurred while moving existing directories. Please close other applications and try again.";

pub const ERR_STDOUT: &str = "An error occurred while writing to stdout.";

pub const ERR_WRITE_CONFIG: &str = "Cannot write config file";

pub const ERR_CONFIG_TO_STRING: &str = "Cannot convert config to string";

pub const WARN_READ_CONFIG: &str =
    "An error occurred while reading configuration file. Fall back to default config";

pub const ERR_FILENAME_UTF8: &str = "Cannot read filename as UTF 8";

pub const ERR_INVALID_REGEX: &str = "Invalid regex";

pub const ERR_INVALID_STORED_REGEX: &str = "Regex stored in config file has to be valid";

pub const PROMPT_USE_REGEX: &str = "Do you want to use a regex?";

pub const ERR_NO_REGEX_SUCCEED: &str = "No regex could parse the file name";
