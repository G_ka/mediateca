use std::path::{Path, PathBuf};

use super::config;
use super::constant::*;
use super::files;
use super::strings;
use super::util::*;

use strings::{
    ERR_CREATE_DIR, ERR_MOVE_EXISTING, PROMPT_DIRECTORY, PROMPT_MOVE_EXISTING,
};

fn setup_dir(path: &Path) -> std::io::Result<()> {
    std::fs::create_dir_all(path.join(STAGING_DIR))?;
    std::fs::create_dir_all(path.join(STORE_DIR))?;
    std::fs::create_dir_all(path.join(DISPLAY_DIR))?;
    Ok(())
}

fn move_existing(path: &Path) -> std::io::Result<()> {
    let iter = std::fs::read_dir(path)?;

    let process = |f: Result<std::fs::DirEntry, std::io::Error>| -> Result<(), Box<dyn std::error::Error>>  {
        let e = f?;
        let path = e.path();
        files::safe_move(&path, files::DirType::Staging, &path)?;
        Ok(())
    };

    for f in iter {
        if let Err(e) = process(f) {
            println!("An error: {}; skipped.", e);
            continue;
        }
    }
    Ok(())
}

pub fn setup(config: &mut config::Config) {
    if config.initialized {
        return;
    }

    config.selected_path = input::<PathBuf>(&PROMPT_DIRECTORY);

    setup_dir(Path::new(&config.selected_path)).expect(ERR_CREATE_DIR);
    if input_bool(&PROMPT_MOVE_EXISTING) {
        move_existing(&config.selected_path).expect(ERR_MOVE_EXISTING);
    }

    config.initialized = true;
}
